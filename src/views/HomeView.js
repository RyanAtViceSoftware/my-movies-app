import { connect } from 'react-redux'
import { Link } from 'react-router'
import MoviePanel from '../components/MoviePanel'
import AddMovieForm from '../components/AddMovieForm'
import { actions as movieActions } from '../redux/modules/movieCollection'

const mapStateToProps = (state) => ({
  movieCollection: state.movieCollection
})

export class HomeView extends React.Component {
  static propTypes = {
    doubleAsync: React.PropTypes.func.isRequired,
    increment: React.PropTypes.func.isRequired,
    movieCollection: React.PropTypes.object.isRequired,
    remove: React.PropTypes.func.isRequired
  }

  render () {
    var movies = this.props.movieCollection.filteredMovies
      .map(movie => {
        return (
            <MoviePanel
              key={movie.id}
              movie={movie}
              onMovieDeleted={this.props.remove}/>
          )
      })

    return (
      <div className='container text-center'>
        <div className='row'>
          <div className='col-md-2'/>
          <div className='col-md-8'>
            <p>This app is a place to store details about your movie collection
            . Find out more on the <Link to='/about'>about</Link> page.</p>
          </div>
        </div>
        <div className='row'>
          <div className='col-md-2'/>
          <div className='col-md-4'>
            {movies}
          </div>
          <div className='col-md-4'>
            <AddMovieForm/>
          </div>
        </div>
        <hr />
        <Link to='/about'>Go To About View</Link>
      </div>
    )
  }
}

export default connect(mapStateToProps, movieActions)(HomeView)
