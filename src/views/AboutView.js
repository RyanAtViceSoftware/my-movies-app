import { Link } from 'react-router'

export class AboutView extends React.Component {
  render () {
    return (
      <div className='container'>
        <div className='col-md-2'/>
        <div className='col-md-8'>
          <h1>About this App</h1>
          <p>This is an app demonstrating React and Redux and a few other Javascript tools. This app was written
           so that I could try out some new tools and also for fun. I'm considering cleaning it up and making it into
          a boiler plate.</p>

          <h3>Technologies</h3>
          This project was based on the <a href='https://github.com/davezuko/react-redux-starter-kit'>React Redux Start Kit</a>
          <ul>
            <li>React</li>
            <li>Redux</li>
            <li>WebPack</li>
            <li>React DevTools (use Ctrl+h and Ctrl+q for working with dev tools window). Sorry PC users...</li>
            <li>Hot Loading</li>
            <li>Redux Forms</li>
          </ul>

          <h3>Features</h3>
          This projects supports the following use cases and developement features.
          <ul>
            <li>Add movies</li>
            <li>Delete Movies</li>
            <li>Search Movies</li>
          </ul>

          <h3>Features left out</h3>
          <ul>
            <li>Unit tests :(</li>
            <li>BaconJs for delaying search by 3 charaters</li>
            <li>Global search, currently you can only search titles</li>
            <li>Persistence, currently it's only has session state</li>
            <li>Async pattern, it'd be nice to have seams that simulate async calls to allow for easier wiring to an API.</li>
          </ul>

          <hr />
          <Link to='/'>Back To Home View</Link>
        </div>
      </div>
    )
  }
}

export default AboutView
