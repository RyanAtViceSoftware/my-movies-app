import '../styles/core.scss'
import { connect } from 'react-redux'
import { actions as movieActions } from '../redux/modules/movieCollection'
import NavBar from '../components/navBar'

const mapStateToProps = (state) => ({
  movieCollection: state.movieCollection
})

class CoreLayout extends React.Component {
  static propTypes = {
    movieCollection: React.PropTypes.object.isRequired
  }

  render () {
    return (
      <div className='page-container'>
        <div className='view-container'>
          <NavBar
            movieCount={this.props.movieCollection.movies.length}/>
          {this.props.children}
        </div>
      </div>
    )
  }
}

CoreLayout.propTypes = {
  children: React.PropTypes.element
}

export default connect(mapStateToProps, movieActions)(CoreLayout)
