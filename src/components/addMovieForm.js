import { reduxForm } from 'redux-form'
import { actions as movieActions } from '../redux/modules/movieCollection'

const fields = ['movieTitle', 'year', 'imageUrl', 'actors', 'rating']

class AddMovieForm extends React.Component {
  static propTypes = {
    fields: React.PropTypes.object.isRequired,
    handleSubmit: React.PropTypes.func.isRequired,
    submitting: React.PropTypes.bool.isRequired
  }

  render () {
    const {
      fields: { movieTitle, year, imageUrl, actors, rating },
      handleSubmit
      } = this.props

    return (
      <form onSubmit={handleSubmit}>
        <div className='well'>
          <h4>Add New Movie</h4>
          <div className='form-group'>
            <input type='text' className='form-control'
              placeholder='Title' {...movieTitle}/>
          </div>
          <div className='form-group'>
            <input type='Number' className='form-control'
              placeholder='Year' {...year}/>
          </div>
          <div className='form-group'>
            <input type='Number' className='form-control'
              placeholder='Rating' {...rating}/>
          </div>
          <div className='form-group'>
            <input type='text' className='form-control'
              placeholder='Actors: Tom Cruise, Nicole Kidman'
              {...actors}/>
          </div>
          <div className='form-group'>
            <input type='url' className='form-control'
              placeholder='Image Url' {...imageUrl}/>
          </div>
          <button className='btn btn-success' onClick={handleSubmit}><small>
            <span className='glyphicon glyphicon-plus'></span></small> Add
          </button>
        </div>
      </form>
    )
  }
}

export default reduxForm({
  form: 'addMovieForm',
  fields
},
null,
{onSubmit: movieActions.add}      // mapDispatchToProps (will bind action creator to dispatch)
)(AddMovieForm)

