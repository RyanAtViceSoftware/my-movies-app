const MoviePanel = (props) => {
  const actors = props.movie.actors.map(actor => {
    return (
        <div>
          <a href='#'>{actor}</a>
        </div>
      )
  })

  const rating = []
  for (var i = 1; i <= 5; i++) {
    if (props.movie.rating >= i) {
      rating.push(<span className='glyphicon glyphicon-star'
          style={{color: 'goldenrod'}}/>)
    } else {
      rating.push(<span className='glyphicon glyphicon-star-empty'/>)
    }
  }

  return (
    <div className='thumbnail' style={{boxShadow: '5px 5px 5px #888888'}}>
      <img src={props.movie.image} />
      <div className='caption'>
        <h3>{props.movie.title}</h3>
        <div className='row'>
          <div className='col-md-4'/>
          <div className='col-md-8 text-left'>
            <div className='row'>
              <div className='col-md-3'>
                <label>Year</label>
              </div>
              <div className='col-md-9'>
                {props.movie.year}
              </div>
            </div>
            <div className='row'>
              <div className='col-md-3'>
                <label>Rating</label>
              </div>
              <div className='col-md-9'>
                {rating}
              </div>
            </div>
            <div className='row'>
              <div className='col-md-3'>
                <label>Cast</label>
              </div>
              <div className='col-md-9'>
                {actors}
              </div>
            </div>
          </div>
        </div>
        <p>
          <button
            className='btn btn-warning'
            onClick={props.onMovieDeleted.bind(null, props.movie.id)}><span className='glyphicon glyphicon-remove'></span> Delete</button>
        </p>
      </div>
    </div>
  )
}

MoviePanel.propTypes = {
  movie: React.PropTypes.object.isRequired,
  onMovieDeleted: React.PropTypes.func.isRequired
}

export default MoviePanel
