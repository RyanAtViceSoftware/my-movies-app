import { reduxForm } from 'redux-form'
import { actions as movieActions } from '../redux/modules/movieCollection'

const fields = ['filter']

class SearchBox extends React.Component {
  static propTypes = {
    filterMovies: React.PropTypes.func.isRequired,
    fields: React.PropTypes.string
  }

  render () {
    return (
      <form className='navbar-form navbar-left' role='search'>
        <div className='form-group'>
          <input type='text' className='form-control'
            placeholder='Search' onChange={this.props.filterMovies}/>
        </div>
      </form>
    )
  }
}

export default reduxForm({
  form: 'movieSearchForm',
  fields
},
null,
{ filterMovies: movieActions.filter }
)(SearchBox)
