import SearchBox from './SearchBox'

const NavBar = (props) => {
  return (
    <nav className='navbar navbar-inverse navbar-fixed-top'>
      <div className='container'>
        <div className='navbar-header'>
          <div className='navbar-brand'>My Movies</div>
        </div>
        <SearchBox/>
        <div className='navbar-text navbar-right'>
          <span className='glyphicon glyphicon-film'>
          <strong>{props.movieCount}</strong></span>
        </div>
      </div>
    </nav>
  )
}

NavBar.propTypes = {
  movieCount: React.PropTypes.number.isRequired
}

export default NavBar
