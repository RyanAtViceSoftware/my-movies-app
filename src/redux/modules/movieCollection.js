import { createAction, handleActions } from 'redux-actions'

// ------------------------------------
// Dummy data
// -------------------------------------
const initialMovies = [
  {
    id: 3,
    genres: ['Action'],
    actors: ['Samuel Jackson', 'Kurt Russell', 'Jennifer Jason Leigh'],
    title: 'Hateful 8',
    year: 2015,
    rating: 5,
    image: 'http://cdn.collider.com/wp-content/uploads/the-hateful-eight-poster1.jpg'
  },
  {
    id: 5,
    genres: ['Commedy'],
    actors: ['Bill Murray', 'Gene Hackman', 'Gwyneth Paltrow', 'Anjelica Huston'],
    title: 'The Royal Tenenbaums',
    year: 2001,
    rating: 4,
    image: 'http://ecx.images-amazon.com/images/I/51rBK15mRTL.jpg'
  },
  {
    id: 1,
    genres: ['Fantsy', 'Commedy'],
    actors: ['Cary Elwes', 'Mandy Patinkin'],
    title: 'The Princess Bride',
    year: 1987,
    rating: 5,
    image: 'https://www.guthriegreen.com/sites/default/files/the-princess-bride.jpg'
  },
  {
    id: 2,
    genres: ['SciFi', 'Action'],
    actors: ['Tom Cruise', 'EmilyBlunt', 'Bill Paxton'],
    title: 'The Edge of Tomorrow',
    year: 2014,
    rating: 4,
    image: 'http://t1.gstatic.com/images?q=tbn:ANd9GcQxMJgknFpnvAXSdXOlU1gi0gDS7KClMOpt6uoGtaTRRJ0AIa5p'
  },
  {
    id: 4,
    genres: ['Commedy'],
    actors: ['Will Smith'],
    title: 'Wild, Wild West',
    year: 1999,
    rating: 1,
    image: 'http://images.moviepostershop.com/wild-wild-west-movie-poster-1999-1020220315.jpg'
  }
]

// ------------------------------------
// Constants
// ------------------------------------
export const MOVIE_ADD = 'MOVIE_ADD'
export const MOVIE_REMOVE = 'MOVIE_REMOVE'
export const FILTER_MOVIES = 'FILTER_MOVIES'

// ------------------------------------
// Actions
// ------------------------------------
export const add = createAction(MOVIE_ADD, (movie) => movie)
export const remove = createAction(MOVIE_REMOVE, (movie) => movie)
export const filter = createAction(FILTER_MOVIES, (movie) => movie)

export const actions = {
  add,
  remove,
  filter
}

// ------------------------------------
// Reducer
// ------------------------------------
const addMovie = (state, { payload }) => {
  const movie = { ...payload,
    id: state.nextId++,
    title: payload.movieTitle,
    image: payload.imageUrl,
    actors: payload.actors.split(', ')
  }

  var allMovies = [...state.movies, movie]

  return {...state,
    movies: allMovies,
    filteredMovies: allMovies
  }
}

const removeMovie = (state, { payload }) => {
  var index = state.movies.map(function (movie) {
    return movie.id
  }).indexOf(payload)

  var allMovies = [
    ...state.movies.slice(0, index),
    ...state.movies.slice(index + 1)
  ]

  return {...state,
    movies: allMovies,
    filteredMovies: allMovies
  }
}

const containsFilter = (value, filter) => {
  if (!value || !filter) return false

  return value.toLowerCase().indexOf(filter.toLowerCase()) > -1
}

const filterMovies = (state, { payload }) => {
  var filteredMovies

  if (payload.target.value) {
    filteredMovies = state.movies.filter(movie => {
      return (containsFilter(movie.title, payload.target.value) ||
        containsFilter(movie.year.toString(), payload.target.value) ||
        containsFilter(movie.actors.join(''), payload.target.value))
    })
  } else {
    filteredMovies = state.movies
  }

  return {...state,
    filteredMovies: filteredMovies
  }
}

export default handleActions({
  [MOVIE_ADD]: addMovie,
  [MOVIE_REMOVE]: removeMovie,
  [FILTER_MOVIES]: filterMovies
}, {
  nextId: initialMovies.lenght + 1,
  movies: initialMovies,
  filteredMovies: initialMovies
})
