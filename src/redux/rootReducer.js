import { combineReducers } from 'redux'
import { routeReducer as router } from 'redux-simple-router'
import counter from './modules/counter'
import movieCollection from './modules/movieCollection'
import { MOVIE_ADD } from './modules/movieCollection'
import { reducer as formReducer } from 'redux-form'

console.debug('addMovieForm MOVIE_ADD' + MOVIE_ADD)

export default combineReducers({
  counter,
  movieCollection,
  form: formReducer.plugin({
    addMovieForm: (state, action) => {
      switch (action.type) {
        case MOVIE_ADD:
          return undefined
        default:
          return state
      }
    }
  }),
  router
})
